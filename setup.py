from setuptools import setup

try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except:
    long_description = ''

setup(
    name='vvclient',
    version='1.0.0',
    packages=['vvclient', 'vedavaapi.objectdb'],
    url='https://github.com/vvclient',
    author='vedavaapi',
    description='Vedavaapi api python client',
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=['requests', 'typing-extensions', 'furl'],
    classifiers=(
            "Programming Language :: Python :: 3.6",
            "Operating System :: OS Independent",
    )
)