from typing import List, Union, Dict, TypeVar, Optional, cast, Any
from typing_extensions import TypedDict

from ...lib.iresource import IResource, Id
from ...lib.context import ModelsRegistry


T = TypeVar('T')


# noinspection PyPep8Naming
class _IResourcesResponse_reqd(TypedDict):
    items: List[IResource]


class IResourcesResponse(_IResourcesResponse_reqd, total=False):
    count: int
    selector_doc: Dict[str, Any]
    total_count: int


# noinspection PyTypedDict
class ResourceResponse(IResourcesResponse):

    @staticmethod
    def make(
            response_body: IResourcesResponse, mr: Optional[ModelsRegistry] = None) -> 'ResourceResponse':
        if mr:
            for item in response_body.get('items', []):
                mr.make(item)
        return cast(ResourceResponse, response_body)

    @staticmethod
    def marshal_middleware(body: Dict[str, Any], mr: Optional[ModelsRegistry] = None):
        # noinspection PyUnresolvedReferences
        return ResourceResponse.make(body, mr)


IResourcesPostResponse = List[IResource]


class IResourcesDeleteResponse(TypedDict):
    all_deleted_ids: List[Id]
    delete_status: Dict[Id, bool]


IResourcesDeleteResponseOld = List[TypedDict('IResourcesDeleteResponseOldItem', {
    'deleted': bool,
    'deleted_resource_ids': List[Id]
})]


# noinspection PyPep8Naming
class _IGraphResponse_reqd(TypedDict):
    graph: Dict[Id, IResource]
    start_nodes_ids: List[Id]


class IGraphResponse(_IGraphResponse_reqd, total=False):
    ool_data_graph: Dict[Id, IResource]
    graph_order: List[str]


# noinspection PyTypedDict
class GraphResponse(IGraphResponse):

    @staticmethod
    def make(response_data: IGraphResponse, mr: Optional[ModelsRegistry] = None) -> 'GraphResponse':
        if mr:
            for _id, res in response_data['graph'].items():
                mr.make(res)
        return cast(GraphResponse, response_data)

    @staticmethod
    def marshal_middleware(body: Dict[str, Any], mr: Optional[ModelsRegistry] = None):
        # noinspection PyUnresolvedReferences
        return GraphResponse.make(body, mr)

    @staticmethod
    def get_reached_resources(gresp: 'GraphResponse', res_or_id: Union[IResource, str], field: str):
        res: IResource
        graph = gresp['graph']
        if isinstance(res_or_id, dict):
            res = res_or_id
            if '_id' not in res:
                return []
        elif res_or_id not in graph:
            return []
        else:
            res = graph[res_or_id]

        rf_ids = res.get('_reached_ids', {}).get(field, [])
        if not rf_ids:
            return []

        return [
            graph[_id]
            for _id in rf_ids
            if _id in graph
        ]


class IGraphPostResponse(TypedDict):
    graph: Dict[Id, Union[IResource, str]]
    ool_data_graph: Dict[Id, Union[str, IResource]]
