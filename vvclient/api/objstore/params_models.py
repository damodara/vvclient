from typing import List, Union, Dict, BinaryIO, Any
from typing_extensions import TypedDict, Literal

from ...lib.iresource import IResource
from ..api_param_models import IProjection, IIDResourceMap, IResourcesGetCommonParams


class IResourcesGetParams(IResourcesGetCommonParams):
    """
    * selector_doc; supports all mongo query syntax
    * @see https://docs.mongodb.com/manual/tutorial/query-documents/
    """
    selector_doc: Dict[str, Any]


# noinspection PyPep8Naming
class _IResourcesPostData_reqd(TypedDict):
    # resources to be posted
    resource_jsons: List[IResource]


class IResourcesPostData(_IResourcesPostData_reqd, total=False):
    return_projection: IProjection
    # should upsert on primary_key match?
    upsert: bool


class IResourcesDeleteData(TypedDict):
    resource_ids: List[str]


# noinspection PyPep8Naming
class _IResGetParams_reqd(TypedDict):
    resource_id: str


IResURLPartParams = _IResGetParams_reqd


class IResGetParams(_IResGetParams_reqd, total=False):
    projection: IProjection
    attach_in_links: bool


# noinspection PyPep8Naming
class _IGraphGetParams_reqd(TypedDict):
    start_nodes_selector: Dict[str, Any]
    direction: Literal['referrer', 'referred']


class IGraphGetParams(_IGraphGetParams_reqd, total=False):
    traverse_key_filter_maps_list: List[Dict[str, Dict[str, Any]]]
    max_hops: int
    hop_inclusions_config: List[Union[bool, str]]
    include_incomplete_paths: bool
    include_ool_data_graph: bool
    json_class_projection_map: Dict[str, IProjection]
    attach_in_links: bool


# noinspection PyPep8Naming
class _IGraphPostData_reqd(TypedDict):
    graph: IIDResourceMap


class IGraphPostData(_IResGetParams_reqd, total=False):
    ool_data_graph: IIDResourceMap
    files: List[Union[str, BinaryIO]]  # TODO
    upsert: bool
    response_projection_map: Dict[str, IProjection]
    should_return_resources: bool
    should_return_oold_resources: bool


class IReferrersSelectorParams(TypedDict):
    resource_id: str
    referrers_type: Literal['specific_resources', 'annotations']


class IReferrersGetParams(IResourcesGetCommonParams, IReferrersSelectorParams, total=False):
    filter_doc: Dict[str, Any]


class IReferrersDeleteData(TypedDict, total=False):
    filter_doc: Dict[str, Any]


class ISchemasURLPartParams(TypedDict):
    json_class: str


class IFilesURLPartParams(TypedDict):
    file_id: str
