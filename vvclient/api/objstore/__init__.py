from typing import Dict
from typing_extensions import Final


from ...lib.context import Context
from ...lib.iresource import IResource
from ...lib import APIEndpoint, get_namespaced_url_gen_factory, IEndpointMethodsConf

from . import response_models as rms
from . import params_models as pms


NAMESPACE_BASE: Final = '/objstore/v1'
url_gen_factory = get_namespaced_url_gen_factory(NAMESPACE_BASE)


class Resources(APIEndpoint[
    Dict,
    pms.IResourcesGetParams, rms.IResourcesResponse,
    None, pms.IResourcesPostData, rms.IResourcesPostResponse,
    None, None, None,
    None, pms.IResourcesDeleteData, rms.IResourcesDeleteResponse
]):
    name = 'objstore.Resources'
    supported_methods = ['get', 'post', 'delete']
    url_gen = url_gen_factory('/resources')

    # noinspection PyUnresolvedReferences
    methods_conf = IEndpointMethodsConf(
        get={
            'middleware': rms.ResourceResponse.marshal_middleware,
        }
    )


class Resource(APIEndpoint[
    pms.IResURLPartParams,
    pms.IResGetParams, IResource,
    None, None, None,
    None, None, None,
    None, None, None
]):
    name = 'objstore.Resource'
    url_part_params = ['resource_id']
    supported_methods = ['get']

    @classmethod
    def _url(cls, vc: Context, url_parts: pms.IResURLPartParams) -> str:
        return f'{vc.base}{NAMESPACE_BASE}/resources/{url_parts["resource_id"]}'


class Graph(APIEndpoint[
    None,
    pms.IGraphGetParams, rms.GraphResponse,
    None, pms.IGraphPostData, rms.IGraphPostResponse,
    None, pms.IGraphGetParams, rms.GraphResponse,
    None, None, None
]):
    name = 'objstore.Graph'
    supported_methods = ['get', 'post', 'put']
    url_gen = url_gen_factory('/graph')

    # noinspection PyUnresolvedReferences
    methods_conf = IEndpointMethodsConf(
        get={
            'middleware': rms.GraphResponse.marshal_middleware
        },
        put={
            'middleware': rms.GraphResponse.marshal_middleware
        },
        post={
            'file_data_params': ['files']
        }
    )


class Referrers(APIEndpoint[
    pms.IReferrersSelectorParams,
    pms.IReferrersGetParams, rms.IResourcesResponse,
    None, None, None,
    None, None, None,
    pms.IReferrersSelectorParams, pms.IReferrersDeleteData, rms.IResourcesDeleteResponse
]):
    name = 'objstore.Referrers'
    url_part_params = ['resource_id', 'referrers_type']
    supported_methods = ['get', 'delete']

    # noinspection PyUnresolvedReferences
    methods_conf = IEndpointMethodsConf(
        get={
            'middleware': rms.ResourceResponse.marshal_middleware,
        }
    )

    @classmethod
    def _url(cls, vc: Context, url_parts: pms.IReferrersSelectorParams) -> str:
        return f'{vc.base}{NAMESPACE_BASE}/resources/{url_parts["resource_id"]}/{url_parts["referrers_type"]}'


class Schemas(APIEndpoint[
    pms.ISchemasURLPartParams,
    pms.ISchemasURLPartParams, IResource,
    None, None, None,
    None, None, None,
    None, None, None
]):
    name = 'objstore.Schemas'
    url_part_params = ['json_class']
    supported_methods = ['get']

    @classmethod
    def _url(cls, vc: Context, url_parts: pms.ISchemasURLPartParams) -> str:
        return f'{vc.base}{NAMESPACE_BASE}/schemas/{url_parts["json_class"]}'


class Files(APIEndpoint[
    pms.IFilesURLPartParams,
    pms.IFilesURLPartParams, None,
    None, None, None,
    None, None, None,
    None, None, None
]):
    name = 'objstore.Files'
    url_part_params = ['file_id']
    supported_methods = ['get']

    @classmethod
    def _url(cls, vc: Context, url_parts: pms.IFilesURLPartParams) -> str:
        return f'{vc.base}{NAMESPACE_BASE}/files/{url_parts["file_id"]}'
