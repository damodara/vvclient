from typing_extensions import TypedDict, Literal


class ITaskResponse(TypedDict):
    status_url: str
    task_id: str


# noinspection PyPep8Naming
class _ITaskStatus_reqd(TypedDict):
    state: Literal['PENDING', 'PROGRESS', 'SUCCESS', 'FAILURE']
    status: str


class ITaskStatus(_ITaskStatus_reqd, total=False):
    current: int
    total: int


class ITaskDeleteStatus(TypedDict):
    success: bool
    task_id: str
