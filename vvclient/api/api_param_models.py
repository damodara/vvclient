from typing import Dict, List, Tuple

from typing_extensions import TypedDict, Literal

from ..lib.iresource import IResource, Id


#  IProjection = Dict[str, Literal[0, 1]]
IProjection = Dict[str, int]  # NOTE; TODO as user have to know about Literal otherwise


IIDResourceMap = Dict[Id, IResource]


class IResourcesGetCommonParams(TypedDict, total=False):
    projection: IProjection
    sort_doc: List[Tuple[str, Literal[1, -1]]]
    start: int
    count: int
    attach_in_links: bool


class ITaskIdParams(TypedDict):
    task_id: str
