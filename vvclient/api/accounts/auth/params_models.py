from typing_extensions import TypedDict, Literal


class IAuthorizationGetParams(TypedDict):
    client_id: str
    response_type: Literal['code', 'token']
    scope: str
    redirect_uri: str
    state: str


class ITokenPostData(TypedDict, total=False):
    client_id: str
    client_secret: str
    code: str
    grant_type: Literal['authorization_code', 'refresh_token', 'implicit', 'client_credentials', 'password']
    redirect_uri: str
    refresh_token: str


class ISignInPostData(TypedDict):
    email: str
    password: str
