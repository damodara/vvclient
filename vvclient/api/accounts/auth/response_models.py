from typing import Union

from typing_extensions import TypedDict, Literal


class _IToken_reqd(TypedDict):
    access_token: str
    token_type: Union[Literal['Bearer'], str]  # NOTE: for now
    expires_in: float


class IToken(_IToken_reqd, total=False):
    refresh_token: str
    scope: str
