import json
from typing import Optional, List

from ..utils import SimpleDict


def get_api_data_and_files(body: SimpleDict, file_data_params: Optional[List[str]] = None):
    data: SimpleDict[str] = {}
    files = []

    file_data_params = file_data_params or []

    for k, v in body.items():
        if v is None:
            continue
        if k not in file_data_params:
            data[k] = json.dumps(v) if not isinstance(v, str) else v
            continue
        k_files = v if isinstance(v, list) else [v]
        files.extend([
            (k, open(file, 'rb') if isinstance(file, str) else file)
            for file in k_files
        ])

    return data, files


def get_api_query_params(params: SimpleDict) -> SimpleDict[str]:
    return get_api_data_and_files(params)[0]


def get_authorized_headers(headers: SimpleDict[str], access_token: Optional[str] = None) -> SimpleDict:
    headers = headers if headers is not None else {}
    authorized_headers = headers.copy()
    if access_token:
        authorized_headers['Authorization'] = f'Bearer {access_token}'
    return authorized_headers
