from typing_extensions import TypedDict
from typing import List, Union, Optional, Any, Dict

from .iresource import IResource


class ModelsRegistry(object):

    def make(self, doc: IResource) -> Any:
        return doc

    def makeFrom(self, doc: Any) -> Any:
        return doc

    @staticmethod
    def default_marshal_middleware(body: Dict[str, Any], mr: 'ModelsRegistry') -> Any:
        return mr.makeFrom(body) if mr else body


class Context(object):

    base: str
    access_token: Optional[str]
    modelsRegistry: Optional[ModelsRegistry]

    def __init__(
            self, base: str, access_token: Optional[str] = None, modelsRegistry: Optional[ModelsRegistry] = None):
        self.base = base.rstrip('/')
        self.access_token = access_token
        self.modelsRegistry = modelsRegistry
