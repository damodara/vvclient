from typing_extensions import TypedDict, Literal
from typing import List, Union, Optional, Any, Dict

Id = str

class IMetadataItem(TypedDict):
    jsonClass: Literal['MetadataItem']
    label: str
    value: Any

'''
class _IResource_reqd(TypedDict):
    jsonClass: str


class IResource(_IResource_reqd, total=False):
    _id: Id
    source: Union[str, List[str]]
    target: Union[str, List[str]]
    metadata: List[IMetadataItem]
    body: Any
    selector: Any
    jsonClassLabel: str
    _reached_ids: Dict[str, List[Id]]
'''

IResource = Dict
