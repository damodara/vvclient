from typing import Optional, List, Callable, TypeVar, Any, Generic, cast, Dict

import requests
from furl import furl, re
from typing_extensions import TypedDict

from .context import Context, ModelsRegistry
from .errors import VVAPIClientError
from .req_utils import get_api_query_params, get_api_data_and_files, get_authorized_headers
from ..utils import SimpleDict


T = TypeVar('T')


class Response(requests.Response, Generic[T]):

    _body: T = None

    def body(self) -> T:
        return self._body

    @classmethod
    def set_body(cls, resp: 'Response'):
        if resp.body():
            return
        if not resp.ok:
            return
        content_type = resp.headers.get('content-type', None)
        is_json = content_type and re.fullmatch(r'^application/(.*\+)?json', content_type)
        if not is_json:
            return
        resp._body = resp.json()

    def json(self, **kwargs) -> T:
        json_body = super(Response, self).json(**kwargs)
        return cast(T, json_body)


def vv_api_request(
        vc: Context,
        url: str,
        method: str = 'get',
        body: Optional[SimpleDict] = None,
        params: Optional[SimpleDict] = None,
        file_data_params: Optional[List[str]] = None,
        headers: Optional[SimpleDict[str]] = None,
) -> Response:
    params = get_api_query_params(params)
    data, files = get_api_data_and_files(body, file_data_params=file_data_params) if body else (None, None)
    authorized_headers = get_authorized_headers(headers, vc.access_token)

    resp = requests.request(method, url, params=params, data=data, files=files, headers=authorized_headers)
    resp.__class__ = Response
    return resp


def get_namespaced_url_gen_factory(namespace_base: str):
    def factory(endpoint: str):
        def url_gen(vc: Context, _url_parts: SimpleDict[str]):
            return f'{vc.base}{namespace_base}{endpoint}'
        return url_gen
    return factory


IURLGen = Callable[[Context, SimpleDict[str]], str]
IResponseMiddleware = Callable[[Dict[str, Any], ModelsRegistry], T]


class IEndpointMethodConf(TypedDict, total=False):
    middleware: IResponseMiddleware
    file_data_params: List[str]


class IEndpointMethodsConf(TypedDict, total=False):
    get: IEndpointMethodConf
    post: IEndpointMethodConf
    put: IEndpointMethodConf
    delete: IEndpointMethodConf


UPP = TypeVar('UPP', bound=SimpleDict[Any])

GP = TypeVar('GP', bound=SimpleDict[Any])
GR = TypeVar('GR')

PP = TypeVar('PP', bound=SimpleDict[Any])
PD = TypeVar('PD')
PR = TypeVar('PR')

TP = TypeVar('TP', bound=SimpleDict[Any])
TD = TypeVar('TD')
TR = TypeVar('TR')

DP = TypeVar('DP', bound=SimpleDict[Any])
DD = TypeVar('DD')
DR = TypeVar('DR')

MP = TypeVar('MP', bound=SimpleDict[Any])
MD = TypeVar('MD')
MR = TypeVar('MR')


# TODO make it an ABC
class APIEndpoint(Generic[UPP, GP, GR, PP, PD, PR, TP, TD, TR, DP, DD, DR], object):

    name = 'APIEndpoint'
    supported_methods = []
    url_part_params = []
    methods_conf: IEndpointMethodsConf = {}
    url_gen: IURLGen = None

    # noinspection PyUnusedLocal
    @classmethod
    def _url(cls, vc: Context, url_parts: SimpleDict[str]) -> str:
        """ sub classes must override """
        return ''

    @classmethod
    def check_method_support(cls, method: str):
        if method.lower() in cls.supported_methods:
            return
        raise VVAPIClientError(f'{method.upper()} is not defined for {cls.name} endpoint')

    @classmethod
    def _compute_url_parts(cls, params: UPP) -> SimpleDict[str]:
        url_parts: SimpleDict[str] = {}
        if not cls.url_part_params:
            return url_parts

        if not params:
            raise VVAPIClientError(f'{cls.name} endpoint requires {cls.url_part_params} params to construct url')

        for param in cls.url_part_params:
            if param not in params:
                raise VVAPIClientError(f'{cls.name} endpoint requires {param} param to construct url')
            url_parts[param] = params[param]

        return url_parts

    @classmethod
    def url(cls, vc: Context, params: Optional[UPP] = None) -> str:
        url_parts = cls._compute_url_parts(params)
        final_params: SimpleDict = None if not params else params.copy()
        if url_parts and final_params:
            for param in url_parts:
                final_params.pop(param)

        full_furl = furl((cls.url_gen or cls._url)(vc, url_parts))
        if final_params:
            query_params = get_api_query_params(final_params)
            full_furl.args.update(query_params)

        return full_furl.url

    @classmethod
    def _get_method_conf(cls, method: str) -> IEndpointMethodConf:
        method = method.lower()
        # noinspection PyTypedDict
        method_conf = cls.methods_conf.get(method, {})
        '''
        if not method_conf:
            raise VVAPIClientError(f'{method.upper()} is not defined for {cls.name} endpoint')
        '''
        return cast(IEndpointMethodConf, method_conf)

    @classmethod
    def request(
            cls, vc: Context, method: str,
            params: Optional[SimpleDict] = None,
            data: Optional[SimpleDict] = None,
            headers: Optional[SimpleDict[str]] = None,
            marshal=True
    ) -> Response:
        cls.check_method_support(method)
        method_conf = cls._get_method_conf(method)
        url = cls.url(vc, params=params)
        response = vv_api_request(
            vc, url, method=method,
            body=data, params=params, file_data_params=method_conf.get('file_data_params'), headers=headers)
        response.raise_for_status()
        Response.set_body(response)
        #  print(response._body)
        if marshal:
            response._body = cls.marshal_response_body(method, response.body(), mr=vc.modelsRegistry)
        return response

    @classmethod
    def marshal_response_body(cls, method: str, body: SimpleDict, mr: Optional[ModelsRegistry] = None) -> Any:
        method_conf = cls._get_method_conf(method)
        middleware = None
        if method_conf and 'middleware' in method_conf:
            middleware = method_conf['middleware']
        elif mr and hasattr(mr, 'default_marshal_middleware'):
            middleware = mr.default_marshal_middleware
        if middleware:
            body = middleware(body, mr)
        return body

    @classmethod
    def marshal_get_response_body(cls, body: SimpleDict, mr: Optional[ModelsRegistry] = None):
        return cls.marshal_response_body('get', body, mr=mr)

    @classmethod
    def marshal_post_response_body(cls, body: SimpleDict, mr: Optional[ModelsRegistry] = None):
        return cls.marshal_response_body('post', body, mr=mr)

    @classmethod
    def marshal_put_response_body(cls, body: SimpleDict, mr: Optional[ModelsRegistry] = None):
        return cls.marshal_response_body('put', body, mr=mr)

    @classmethod
    def marshal_delete_response_body(cls, body: SimpleDict, mr: Optional[ModelsRegistry] = None):
        return cls.marshal_response_body('delete', body, mr=mr)

    @classmethod
    def get(
            cls, vc: Context,
            params: Optional[GP] = None, headers: Optional[SimpleDict[str]] = None,
            marshal=True) -> Response[GR]:
        return cast(GR, cls.request(vc, 'get', params=params, headers=headers, marshal=marshal))

    @classmethod
    def post(
            cls, vc: Context,
            params: Optional[PP] = None, data: Optional[PD] = None, headers: Optional[SimpleDict[str]] = None,
            marshal=True) -> Response[PR]:
        return cast(PR, cls.request(vc, 'post', params=params, data=data, headers=headers, marshal=marshal))

    @classmethod
    def put(
            cls, vc: Context,
            params: Optional[TP] = None, data: Optional[TD] = None, headers: Optional[SimpleDict[str]] = None,
            marshal=True) -> Response[TR]:
        return cast(TR, cls.request(vc, 'put', params=params, data=data, headers=headers, marshal=marshal))

    @classmethod
    def delete(
            cls, vc: Context,
            params: Optional[DP] = None, data: Optional[DD] = None, headers: Optional[SimpleDict[str]] = None,
            marshal=True) -> Response[DR]:
        return cast(DR, cls.request(vc, 'delete', params=params, data=data, headers=headers, marshal=marshal))
