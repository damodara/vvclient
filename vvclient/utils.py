from enum import Enum, unique
from typing import Dict, TypeVar

T = TypeVar('T')
SimpleDict = Dict[str, T]


@unique
class Empty(Enum):
    token = 0


empty = Empty.token
