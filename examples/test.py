from vvclient.lib.context import Context
from vvclient.api import objstore
from vvclient.api.objstore.params_models import IGraphGetParams

vc = Context('https://services.vedavaapi.org:8443/api', access_token='6owACQk8qWMiuSFW70Ji4pvYKrDNoy')

resp = objstore.Graph.get(
    vc,
    params=IGraphGetParams(
        start_nodes_selector={
            "jsonClass": "ScannedPage",
            "_id": "5dc1466d549591000e5ff4e7"
        },
        direction='referrer',
        traverse_key_filter_maps_list=[
            {"source": {"jsonClass": "ImageRegion"}},
            {"target": {"jsonClass": "TextAnnotation"}}
        ],
        max_hops=2,
        json_class_projection_map={"*": {"resolvedPermissions": 0}}
    ),
    marshal=True
)


schema = objstore.Schemas.get(
    vc,
    params={
        'json_class': 'Annotation'
    }
)